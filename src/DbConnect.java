
import java.sql.*;

public class DbConnect {
    //Instansvariabel som hjälper mig att connecta till min DB
    private Connection con;
    private Statement st;
    private ResultSet rs;
    
    
    public DbConnect(){
        try{
            
            //Drivrutin som möjliggör åtkomst till min DB
            Class.forName("com.mysql.jdbc.Driver");
            // Connectar till min DB
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/pokerspelare", "root", "");
            st = con.createStatement();
        
        //Fångar eventuella fel
        }catch(Exception ex){
            System.out.println("Error"+ex);
    
        }
    
    }
    public void getData(){
        try{
            
            String query = "select * from spelare";
            rs = st.executeQuery(query);
            System.out.println("Records from Database");
            while(rs.next()){
                
                //Hämtar data från DB och lägger dem i variabler
                int spelarId = rs.getInt("SpelarID");
                String name = rs.getString("Namn");
                int result =rs.getInt("Resultat");
                
                //Skriver ut text och variabler som håller värde på data i de olika kolumnerna i min DB
                System.out.println("ID: "+spelarId+" | Namn: "+ name +" | Resultat: "+ result);
            }
            
            
        }catch(Exception ex){
            System.out.println("Error"+ex);
        }
    }
}
